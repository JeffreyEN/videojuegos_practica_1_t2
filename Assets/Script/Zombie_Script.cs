using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie_Script : MonoBehaviour
{
    private Animator _animator;
    private SpriteRenderer sr;
    private Rigidbody2D rb2d;
    public Pj_script Pj_Script;

    public int VelocidadDeMovimiento;
    public float upSpeed = 60;
    public float RunSpeed = 10;
    private bool contador = true;
    private bool EstaTocandoElSuelo = false;
    public float TimeT;
    private float Runtime = 0f;

    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Pj_Script.muerto == false)
        {
            Runtime += Time.deltaTime;
            if (Runtime < TimeT)
            {
                sr.flipX = false;
                rb2d.velocity = new Vector2(RunSpeed, rb2d.velocity.y);
            }
            if (Runtime > TimeT)
            {
                sr.flipX = true;
                rb2d.velocity = new Vector2(-RunSpeed, rb2d.velocity.y);
            }
            if (Runtime >= TimeT * 2)
                Runtime = 0f;
        }
        else
        {
            rb2d.velocity = new Vector2(0, rb2d.velocity.y);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 9)
        {
            Pj_Script.Puntaje += 10;
            Destroy(collision.gameObject);
            Destroy(this.gameObject);

        }
    }
}