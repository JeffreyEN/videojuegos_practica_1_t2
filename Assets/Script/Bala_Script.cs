using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala_Script : MonoBehaviour
{
    private Rigidbody2D rb2d;
    private SpriteRenderer sr;
    private Pj_script Pj_Script;
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        Destroy(this.gameObject, 5);

    }
    void Update()
    {
        if (sr.flipX)
        {  
            rb2d.velocity = new Vector2(-15f, rb2d.velocity.y);
        }
        else
        {
            rb2d.velocity = new Vector2(15f, rb2d.velocity.y);
        }

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 3)
        {
            Pj_Script.Puntaje += 10;
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
            
        }
    }
}

