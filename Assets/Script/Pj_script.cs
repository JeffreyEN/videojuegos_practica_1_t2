using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pj_script : MonoBehaviour
{
    private Animator _animator;
    private SpriteRenderer sr;
    private Rigidbody2D rb2d;
    private Transform _transform;

    public GameObject Bala_i;
    public GameObject Bala_d;

    public float upSpeed = 60f;
    public float RunSpeed = 10f;
    private int N_Monedas_Bronze = 0;
    private int N_Monedas_Plata = 0;
    private int N_Monedas_Oro = 0;
    public int Puntaje = 0;

    public bool muerto = false;
    private bool EstaTocandoElSuelo = false;

    public Text M_Bronze;
    public Text M_Plata;
    public Text M_Oro;
    public Text M_Puntaje;


    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        _transform = GetComponent<Transform>();
    }

    void Update()
    {
        Debug.Log("puntaje: "+ Puntaje);
        M_Puntaje.text = "PUNTAJE: " + Puntaje;
        M_Bronze.text = "X " + N_Monedas_Bronze;
        M_Plata.text = "X " + N_Monedas_Plata;
        M_Oro.text = "X " + N_Monedas_Oro;
        if (muerto == false)
        {

            if (Input.GetKey(KeyCode.RightArrow))
            {
                sr.flipX = false;
                rb2d.velocity = new Vector2(RunSpeed, rb2d.velocity.y);
                setRunAnimation();
                if (Input.GetKey(KeyCode.Z))
                {
                    setSlideAnimation();
                    rb2d.velocity = new Vector2(RunSpeed+10, rb2d.velocity.y);
                }
            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                sr.flipX = true;
                rb2d.velocity = new Vector2(-RunSpeed, rb2d.velocity.y);
                setRunAnimation();
                if (Input.GetKey(KeyCode.Z))
                {
                    setSlideAnimation();
                    rb2d.velocity = new Vector2(-RunSpeed-10, rb2d.velocity.y);
                }
            }
            else
            {
                rb2d.velocity = new Vector2(0, rb2d.velocity.y);
                setIdleAnimation();
            }
            if (Input.GetKeyDown(KeyCode.Space) && EstaTocandoElSuelo)
            {
                setJumpAnimation();
                rb2d.velocity = Vector2.up * upSpeed;
                EstaTocandoElSuelo = false;
            }
            
            if(Input.GetKeyDown(KeyCode.X))
            {
                if (sr.flipX)
                {
                    
                    var BulletPosition = new Vector3(_transform.position.x - 1f, _transform.position.y, _transform.position.z);
                    Instantiate(Bala_i, BulletPosition, Quaternion.identity);
                }
                if (!sr.flipX)
                {
                    var BulletPosition = new Vector3(_transform.position.x + 1f, _transform.position.y, _transform.position.z);
                    Instantiate(Bala_d, BulletPosition, Quaternion.identity);
                }

            }
        }
        else
        {
            setJDeadAnimation();
            rb2d.velocity = new Vector2(0, rb2d.velocity.y);
        }
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        EstaTocandoElSuelo = true;

        if (other.gameObject.layer == 3)
        {
            muerto = true;
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == 6)
        {
            Destroy(other.gameObject);
            N_Monedas_Bronze += 1;
        }
        else if (other.gameObject.layer == 7)
        {
            Destroy(other.gameObject);
            N_Monedas_Plata += 1;
        }
        else if (other.gameObject.layer == 8)
        {
            Destroy(other.gameObject);
            N_Monedas_Oro += 1;
        }
    }

    private void setIdleAnimation()
    {
        _animator.SetInteger("Estado", 0);
    }
    private void setRunAnimation()
    {
        _animator.SetInteger("Estado", 1);
    }
    private void setJumpAnimation()
    {
        _animator.SetInteger("Estado", 2);
    }
    private void setSlideAnimation()
    {
        _animator.SetInteger("Estado", 3);
    }
    private void setJDeadAnimation()
    {
        _animator.SetInteger("Estado", 4);
    }

}

